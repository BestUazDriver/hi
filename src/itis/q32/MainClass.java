package itis.q32;


/*
В папке resources находятся два .csv файла.
Один содержит данные о группах в университете в следующем формате: ID группы, название группы, код группы
Второй содержит данные о студентах: ФИО, дата рождения, айди группы, количество очков рейтинга

напишите код который превратит содержимое файлов в обьекты из пакета "entities", выведите в консоль всех студентов,
в читабельном виде, с информацией о группе
Используя StudentService, выведите:

1. Число групп с только совершеннолетними студентами
2. Самую маленькую группу
3. Отношение группа - сумма балов студентов фамилия которых совпадает с заданной строкой
4. Отношения студент - дельта баллов до проходного порога (порог передается параметром),
 сгруппированные по признаку пройден порог, или нет

Требования к реализации: все методы в StudentService должны быть реализованы с использованием StreamApi.
Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
*/

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class MainClass {

    private StudentService studentService = new StudentServiceImpl();

    public static void main(String[] args) throws IOException {
        new MainClass().run(
                "C:\\Users\\admin\\IdeaProjects\\src\\q32-students\\src\\itis\\q32\\resources\\students.csv",
                "C:\\Users\\admin\\IdeaProjects\\src\\q32-students\\src\\itis\\q32\\resources\\groups.csv");
    }

    private void run(String studentsPath, String groupsPath) throws IOException {
        BufferedReader studentsReader = new BufferedReader(new FileReader(studentsPath));
        BufferedReader groupsReader = new BufferedReader(new FileReader(groupsPath));
        ArrayList<Student> students = new ArrayList<>();
        String line;
        while ((line = studentsReader.readLine()) != null) {
            String[] atributes = line.split(", ");
            Student student = new Student();
            student.setFullName(atributes[0]);
            Group group = new Group();
            group.setId(Long.parseLong(atributes[2]));
            student.setGroup(group);
            student.setScore(Integer.parseInt(atributes[3]));
            String[] date = atributes[1].split("-");
            student.setBirthdayDate(LocalDate.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])));
            students.add(student);
            System.out.println(student.toString());
        }
        System.out.println(" ");
        ArrayList<Group> groups = new ArrayList<>();
        String line2;
        while ((line2 = groupsReader.readLine()) != null) {
            String[] atributes = line2.split(", ");
            Group group = new Group();
            group.setId(Long.parseLong(atributes[0]));
            group.setTitle(atributes[1]);
            group.setCode(atributes[2]);
            groups.add(group);
            System.out.println(group);
        }
    }
}










