package itis.q32;

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Stream;

public class StudentServiceImpl implements StudentService {


    @Override
    public Group getSmallestGroup(List<Student> students) {
        Stream stream =students.stream();
        ArrayList<Group> groups = new ArrayList<>();
        for (int i=0;i<students.size(); i++){
            students.get(i).getGroup().getId();
        }
        return null;
    }

    @Override
    public Integer countGroupsWithOnlyAdultStudents(List<Student> students) {
        return null;
    }

    @Override
    public Map<String, Integer> getGroupScoreSumMap(List<Student> students, String studentSurname) {
        Map<String, Integer> map = new HashMap<>();
        int total=0;
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getFullName()==studentSurname){
                total=total+students.get(i).getScore();
                map.put(students.get(i).getGroup().getTitle(), total);
            }
        }
        return map;
    }

    @Override
    public Map<Boolean, Map<String, Integer>> groupStudentScoreWithThreshold(List<Student> students, Integer threshold) {
        HashMap<String, Integer> map1 = new HashMap<>();
        HashMap<String, Integer> map = new HashMap<>();
        HashMap<Boolean, Map<String,Integer>> map2 = new HashMap<>();
        for (int i = 0; i < students.size(); i++) {
            int total=students.get(i).getScore()-threshold;

            if (total>0){
                map1.put(students.get(i).getFullName(), Math.abs(total));
                map2.put(true, map1);
            }else{
                map.put(students.get(i).getFullName(), Math.abs(total));
                map2.put(false, map);
            }
        }
        return map2;
    }
}
